function each(elements, callBack) {
    if (elements === undefined || elements.length === 0) {
        return [];
    }
    for (let i = 0; i < elements.length; i++) {
        callBack(i, elements[i]);
    }
}

module.exports = each;
