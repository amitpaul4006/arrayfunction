function filter(elements, callBack) {
  if (elements === undefined || elements.length === 0) {
    return [];
  }
  let val = [];
  for (let i = 0; i < elements.length; i++) {
    if (callBack(elements[i])) {
      val.push(elements[i]);
    }

  }
  return val;
}
module.exports = filter;