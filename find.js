function find(elements, callBack) {
  if (elements === undefined || elements.length === 0) {
    return [];
  }
  let val = undefined;
  for (let i = 0; i < elements.length; i++) {
    if (callBack(elements[i])) {
      val = elements[i];
      return val;
    }

  }
  return val;
}
module.exports = find;