let resarr = [];
function flatten(elements) {
    if (elements === undefined || elements.length === 0) {
        return [];
    }
    for (var i = 0; i < elements.length; i++) {
        if (typeof elements[i] == 'object') {
            // console.log(elements[i]);
            flatten(elements[i]);
        }
        if (typeof elements[i] == 'number') {
            resarr.push(elements[i])
        }

    }
    return resarr;

}
module.exports = flatten;