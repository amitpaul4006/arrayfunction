function map(elements, callBack) {
  if (elements === undefined || elements.length === 0) {
    return [];
  }
  let arr = []
  for (let i = 0; i < elements.length; i++) {
    if (elements[i])
      arr.push(callBack(elements[i], i, elements));
  }
  return arr;
}
module.exports = map;