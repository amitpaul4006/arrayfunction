function reduce(elements, callBack, start) {
  let cpy_elements = elements;
  if (elements === undefined || elements.length === 0) {
    return [];
  }
  if (start === undefined) {
    start = cpy_elements[0];
    cpy_elements = cpy_elements.slice(1);
  }
  let val = start;
  for (let i = 0; i < cpy_elements.length; i++) {
    if (cpy_elements[i])
      val = callBack(val, cpy_elements[i], i, elements);
  }
  return val;
}
module.exports = reduce;
