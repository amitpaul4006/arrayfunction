const { items } = require('../data');
const each = require('../each');

function callBack(i, j) {
    console.log(i, j);
}

const result = each(items, callBack);
if (result) {
    console.log(result);

}