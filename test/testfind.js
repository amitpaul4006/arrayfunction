const { items } = require('../data');
const find = require('../find');

function callBack(val) {
  if (val > 3) {
    return true;
  }
  else {
    return false;
  }
}
let result = find(items, callBack);
if (result === undefined) {
  console.log("Condition doesn't exists");
}
else {
  console.log(result);
}
